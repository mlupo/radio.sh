#!/bin/bash

input=""

radiostations=(
	"Klassik Radio"
	"SWR4 HN"
	"BR Klassik" 
	"Bayern 1" 
	"BR24" 
	"Deutschlandfunk" 
	"Polskie Radio Dwójka" 
	"Radio Pyongyang"
)

radiosources=(
	"https://edge52.streams.klassikradio.de/klassikradio-deutschland"
	"https://dispatcher.rndfnk.com/swr/swr4/hn/aac/96/stream.aac?aggregator=radio-de"
	"https://dispatcher.rndfnk.com/br/brklassik/live/aac/low"
	"https://dispatcher.rndfnk.com/br/br1/franken/aac/low"
	"https://dispatcher.rndfnk.com/br/br24/live/aac/low"
	"https://st01.sslstream.dlf.de/dlf/01/low/aac/stream.aac"
	"http://stream3.polskieradio.pl:8902/;stream.mp3"
	"https://listen7.myradio24.com/69366"
)

echo "Choose a radio station"

count=0

for radiostation in "${radiostations[@]}"
do
  echo "($((count + 1))): $radiostation"
  count=$((count + 1))
done

echo "Please select a Number: "
read -r input

if [ "$input" -gt "$count" ]; then
  echo "You picked the wrong number!"
  exit
fi

mpv "${radiosources[$((input - 1))]}"

echo "Thank you for listening!"
